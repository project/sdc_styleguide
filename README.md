## INTRODUCTION

The goal of this module is to provide a quick interface to test single directory
components without having to actually create content on the site.

In an ideal world, front end developers can build the markup, styles and
behaviors for the components and showcase without fully integrating them into
Drupal, similar to Patternlab or Storybook.

## REQUIREMENTS

* Drupal 10
* Single Directory Components

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## USAGE
* Go to /styleguide/explorer
* Navigate using the component explorer

## MAINTAINERS

Current maintainers for Drupal 10:

- Alejandro Madrigal (alemadlei) - https://www.drupal.org/u/alemadlei
