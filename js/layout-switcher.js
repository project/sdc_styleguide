(() => {

  'use strict';

  Drupal.behaviors.sdcStyleguideLayoutSwitcherInit = {
    attach: (context, settings) => {
      const switcher = once('layout-switcher-init', '.layout-switcher__trigger', context);
      if (switcher.length == 0) {
        return;
      }

      const page = document.querySelector('.sdc-styleguide-page');
      page.classList.add('sdc-styleguide-page--alternate-layout');
      switcher[0].addEventListener('click', e => {
        page.classList.toggle('sdc-styleguide-page--alternate-layout');
      });
    }
  };

})();
