<?php

namespace Drupal\sdc_styleguide\Service;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ComponentPluginManager;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class to manage demos.
 */
class SDCDemoManager {
  use StringTranslationTrait;
  use MessengerTrait;

  private static $supportedTypes = [
    'boolean',
    'number',
    'string',
    'array',
    'object',
  ];

  /**
   * The list of demos.
   *
   * The array contains two indexes:
   *   - index: All demos accessible by name.
   *   - groups: All demos accessible by SDC group.
   *
   * @var array
   */
  private $demos = [];

  /**
   * Constructs a new SDCDemoManager object.
   */
  public function __construct(
    private readonly ComponentPluginManager $pluginManagerSdc,
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly ThemeManagerInterface $themeManager,
  ) {}

  /**
   * Gets a demo by id.
   *
   * @param string $id
   *   The id of the demo.
   *
   * @return mixed|null
   *   The demo with the matching id or NULl if not found.
   */
  public function getDemoById($id) {
    if (empty($this->demos)) {
      $this->getDemos();
    }
    return $this->demos['index'][$id] ?? NULL;
  }

  /**
   * Gets a list of all available SDC and their demos if available.
   *
   * @return array
   *   The list of demos grouped by Group and Component.
   */
  public function getDemos() {
    if (!empty($this->demos)) {
      return $this->demos['groups'];
    }

    // Avoids having to regenerate all demos in the same request.
    // @todo Maybe add cache?
    $this->demos = [
      'groups' => [],
      'index' => [],
    ];

    // Builds demos.
    $componentDemos = &$this->demos['groups'];
    $ungroupedIndex = $this->t('Ungrouped')->render();
    foreach ($this->pluginManagerSdc->getAllComponents() as $component) {
      $definition = $component->getPluginDefinition();
      $group = $definition['group'] ?? $ungroupedIndex;
      $componentId = $component->getPluginId();

      if (!isset($componentDemos[$group])) {
        $componentDemos[$group] = [];
      }

      // Adds component.
      $componentDemos[$group][$componentId] = [
        'name' => $definition['name'],
        'demos' => [],
      ];
      $demos = &$componentDemos[$group][$componentId]['demos'];

      // Finds demos for the current component and adds them to the explorer.
      $finder = new Finder();
      $component_name = $definition['machineName'];
      $finder->in($definition['path'])->files()->name("{$component_name}.demo.*.yml");
      foreach ($finder as $file) {
        $key = str_replace(['.yml', "{$component_name}.demo."], '', $file->getFilename());
        $contents = $file->getContents();
        $demo_data = Yaml::decode($contents);
        $demo_data['component_id'] = $componentId;

        // Attribute value validation. Required for when we have
        if (!empty($definition['props']['properties'])) {
          foreach ($definition['props']['properties'] as $prop_name => $prop_settings) {
            $originalValue = &$demo_data['props'][$prop_name];

            if (!in_array($prop_settings['type'], self::$supportedTypes)) {
              $newValue = NULL;
              $this->moduleHandler->alter('styleguide_demo_convert_stored_value_to_complex_type',$newValue, $originalValue, $prop_settings);
              $this->themeManager->alter('styleguide_demo_convert_stored_value_to_complex_type',$newValue, $originalValue);
              if (empty($newValue) & !empty($originalValue)) {
                $this->messenger()->addWarning($this->t('Cannot read value from demo file for property @prop type @type.', [
                  '@prop' => $prop_settings['title'],
                  '@type' => $prop_settings['type'],
                ]));
              }

              $demo_data['props'][$prop_name] = $newValue;
              continue;
            }

            if ($prop_settings['type'] == 'string') {
              if (trim($originalValue) != trim(strip_tags($originalValue))) {
                $originalValue = [
                  '#type' => '#markup',
                  '#markup' => $originalValue,
                ];
              }

              continue;
            }
            else if ($prop_settings['type'] == 'number') {
              $int = filter_var($originalValue, FILTER_VALIDATE_INT);
              $originalValue = $int != FALSE ? $int : (double)$originalValue;
            }
            else if ($prop_settings['type'] == 'array') {
              if (empty($originalValue)) {
                $originalValue = [];
              }
            }

            $demo_data['props'][$prop_name] = $originalValue;
          }
        }

        $demos[$key] = $demo_data;
        $this->demos['index']["{$componentId}.demo.{$key}"] = $demo_data;
      }

      // Sorts by component readable name.
      uasort($demos, fn ($a, $b) => strcmp($a['name'], $b['name']));
    }

    foreach ($componentDemos as &$group) {
      uasort($group, fn($a, $b) => strcmp($a['name'], $b['name']));
    }
    ksort($componentDemos);
    return $componentDemos;
  }

}
